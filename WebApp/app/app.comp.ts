import "bootstrap"
import "jquery"

import { Component } from '@angular/core';
import {ProducerApi} from "./producer-api.service";

@Component({
  selector: 'my-app',
  template: `
    <alert-display [alertMessage]="setAlertMessage"></alert-display>
    <h1>Minute Lab Kafka Producer</h1>
    
    <div style="width: 400px"> 
        <div class="form-group">
            <label>User name:</label>
            <input type="text" [(ngModel)]="userName">
        </div>
        <div class="form-group">
            <label>Amount of message:</label>
            <input type="text" [(ngModel)]="numMessages" >
        </div>
    </div>
    <button (click)="sendMessages()" >Send messages</button>
`
  ,

})
export class AppComponent  {
    constructor(
        private producerApi:ProducerApi
    ){

    }
    setAlertMessage:()=>string
    userName:string="minutelab";
    name = 'Angular';

    numMessages:number=100;

    sendMessages=()=>{
        this.producerApi.sendMessages({userName:this.userName,numMessages:this.numMessages}).subscribe(res=>{
            console.log(res.json())
            this.setAlertMessage=()=>"Messages sent"
        })
    }


}
