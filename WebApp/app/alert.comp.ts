/**
 * Created by odedl on 15-Mar-17.
 */
import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';


@Component({
    selector: 'alert-display',
    template: `
    <div class="container">
        <div class="row" style="margin-top: 40px">
            <div class="span12">  
                <div class="alert alert-success" *ngIf="_alertMessage">
                    <a class="close" data-dismiss="alert" aria-label="close" (click)="removeAlertMessage()">&times;</a>
                    <strong>Note: </strong>{{_alertMessage}}
                </div>
            </div>
        </div>
    </div>
`
})
export class AlertDisplay implements OnInit {
    constructor() {
    }

    ngOnInit() {
    }

    @Output()
    requestClearError: EventEmitter<any> = new EventEmitter<any>();

    _alertMessage: string

    @Input()
    set alertMessage(val: () => string) {
        if(val) {
            this._alertMessage = val();
        }
    }

    removeAlertMessage = () => {
        this._alertMessage = "";
    }
}
/**
 * Created by odedl on 08-May-17.
 */
