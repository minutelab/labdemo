import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.comp';
import {ProducerApi} from "./producer-api.service";
import {CommonModule} from "@angular/common";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {AlertDisplay} from "./alert.comp";

@NgModule({
  imports:      [ BrowserModule,CommonModule,HttpModule ,FormsModule],
  declarations: [ AppComponent,AlertDisplay ],
  providers:    [ProducerApi],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
