/**
 * Created by odedl on 08-May-17.
 */
import {Injectable} from '@angular/core';
import {Http, RequestOptions, RequestOptionsArgs,Headers} from "@angular/http";
import {configuration} from "./config";
import {IMessageMeta} from "../../Common/dataModels/messageMeta";


@Injectable()
export class ProducerApi {
    private SEND_MESSAGES_URL="/producer/sendMessages"
    constructor(private http:Http) {
    }

    private getFullUrl=(url:string):string=>{
        return configuration.baseUrl + url;
    }

    private getHeaders=()=>{
        let headers = new Headers({'Content-Type': 'application/json'});
        return {headers}
    }
    sendMessages=(meta:IMessageMeta)=>{
        let url = this.getFullUrl(this.SEND_MESSAGES_URL)
        return this.http.post(url,meta,this.getHeaders())
    }
}