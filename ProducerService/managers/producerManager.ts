/**
 * Created by odedl on 08-May-17.
 */
import {Producer,Client} from "kafka-node"
import {IMessageMeta} from "../../Common/dataModels/messageMeta";

export class ProducerManager{
    counter =0;
    producer:Producer;
    topic:string;
    kafkaBroker:string;
    constructor(){
        this.topic=process.env.TOPIC || "test"
        this.kafkaBroker=process.env.BROKER || "localhost:2181";
        this.initProducer();
    }

    initProducer =()=>{
        let client = new Client(this.kafkaBroker,"minutelab-producer")
        this.producer = new Producer(client)
        this.producer.on("ready",()=>{
            console.log("producer ready");
            this.createTopic()
        })
        console.log(`working with kafka brokers : ${this.kafkaBroker}`)

    }

    createTopic=()=>{
        this.producer.createTopics([this.topic],false,(err,added)=>{
            if(err){
                console.log(err)
            }else if(added){
                console.log(`In producer added topic ${added}`)
            }
        })
    }

    sendMessages=(messages:any[])=>{
        let producerMessages = [];
        producerMessages.push({
            topic: this.topic,
            messages, // multi messages should be a array, single message can be just a string,
        })
        this.producer.send(producerMessages,(err,data)=>{
            if(err){
                console.log(err)
            }else{
                console.log(data)
            }
        })
    }

    sendMessagesFromMeta=async (meta:IMessageMeta)=>{
        return new Promise<any>((resolve,reject)=>{
            if(!meta){
                reject();
            }
            let messages = [];
            for(let i=0;i<meta.numMessages;i++){
                messages.push( JSON.stringify({index:this.counter++, userName:meta.userName,newContent:"something new"})) //,newContent:"something new"
                if(messages.length>=10 ) {
                    this.sendMessages(messages)
                    messages = [];
                }
            }
            //send remaining in queue
            if(messages.length>0)
                this.sendMessages(messages)
            resolve(null)
        })

    }
}