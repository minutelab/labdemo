/**
 * Created by odedl on 08-May-17.
 */
import {Router} from "express";
import * as express from "express";
import {ProducerManager} from "../managers/producerManager";

export class ProducerRouter{
    router: Router;
    producerManager:ProducerManager

    constructor() {
        this.router = express.Router();
        this.createRoutes();
        this.producerManager = new ProducerManager()
    }

    private createRoutes() {
        this.router.post("/sendMessages", this.sendMessages);

    }

    private sendMessages = (req, res) => {
        this.producerManager.sendMessagesFromMeta(req.body).then(result=>{
            res.status(200).send({res:"sent messages"})
        }).catch(err=>{
            res.status(500).send(err)
        })

    }

}

export const producerRouter= new ProducerRouter().router;