/**
 * Created by odedl on 08-May-17.
 */
export interface IMessageMeta{
    userName:string
    numMessages:number
}