Interactive Node.js + Kafka + Zoo Keepr
=======================================

This demo include interactive development demo that show node + kafka + zookeeper

## Demo content

The simulated environment include:

 * Producer - a node.js application that in response to web request posts messages to Kafka queue
 * Kafka cluster - two broker cluster
 * Zookeeper cluster - three nodes cluster
 * Consumer -  a node.js application that periodically read from the Kafka cluster and write to files

The whole lab is started with the `devlab.mlab` script, which start the two clusters, and two node.js applications.

## Interactive code changes
The node.js applications are started with `tsc -w` and [nodemon](https://github.com/remy/nodemon) so every code change
immediately reload the applications.

Minute Lab synchronization technology make sure that every code change on the dekstop will be synchronized to the container.

The result is that when you save the code you can immediately test the implications.


## Full checkout from git

This repository relies on mlab scripts from the [main MinuteLab demo repository](https://github.com/minutelab/demos).
They appear in this repo as a [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

After checkout one should checkout out also the submodule using:

```
git submodule init
git submodule update
```
