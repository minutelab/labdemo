import {ConsumerManager} from "./managers/consumerManager";
/**
 * Created by odedl on 08-May-17.
 */



export class Server{

    _consumerManager:ConsumerManager;
    constructor(){
    }

    public startServer=()=>{
        this.initConsumer()
    }

    private initConsumer() {
        this._consumerManager=new ConsumerManager();
    }
}