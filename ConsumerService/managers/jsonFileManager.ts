/**
 * Created by odedl on 14-May-17.
 */
//manager for saving json content to file
export class JsonFileManager{
    rootFileLocation:string;
    constructor(){
        this.rootFileLocation=process.env.OUTPUT_FOLDER_PATH ||"/consumer-output"
    }

    saveJson=(jsonContent:any,fileName:string):{fileName:string,fullPath:string}=>{
        var jsonfile = require('jsonfile')
        var uuid = require('uuid');

        var basePath = `${this.rootFileLocation}`;
        this.ensurePathExists(basePath);
        let fullFileName  = `${fileName}.${uuid.v4()}.json`;
        let fullPath = `${basePath}/${fullFileName}`
        jsonfile.writeFileSync(fullPath, jsonContent, function (err) {
            if(err){
                console.error('error when saving content ${file}',err)
            }
        })
        return {fileName:fullFileName,fullPath:fullPath};
    }

    private ensurePathExists=(path:string)=>{
        var fs = require('fs');

        if (!fs.existsSync(path)){
            fs.mkdirSync(path);
        }
    }
}