/**
 * Created by odedl on 08-May-17.
 */
class HdfsManager{
    hdfs;
    hdfsHost:string;
    hdfsUser:string;
    constructor(){

        this.hdfsUser = process.env.HDFS_USER||"odedl"; //spark
        this.hdfsHost = process.env.HDFS_HOST||"localhost" //master-spark
        console.log(`hdfsHost ${this.hdfsHost}`)
        this.initHdfs()
    }

    private initHdfs() {
        let WebHDFS = require('webhdfs');
        this.hdfs = WebHDFS.createClient({
            user: this.hdfsUser,
            host: this.hdfsHost,
            port: 50070,
            path: '/webhdfs/v1'
        });
        console.log(`creating hdfs client with config ${JSON.stringify(this.hdfs)}`)
    }

}
export const hdfs = new HdfsManager().hdfs

export class HdfsActions{
    static exportFile=(localFilePath:string,remotePath)=>{
        var fs = require('fs');
        var localFileStream = fs.createReadStream(localFilePath); //'C:/Temp/oded.txt'
        var remoteFileStream = hdfs.createWriteStream(remotePath);//'/name/test/oded.new.txt'
        localFileStream.pipe(remoteFileStream);

        remoteFileStream.on('error', function onError (err) {
            console.error(`error passing file ${localFilePath} to to hdfs remote ${remotePath}`,err)
        });

        remoteFileStream.on('finish', function onFinish () {
            console.log(`finished passing file ${localFilePath} to to hdfs remote ${remotePath}`)
        });
    }

    static readFile=(hdsfFile:string)=>{
        var remoteFileStream = hdfs.createReadStream(hdsfFile);

        remoteFileStream.on('error', function onError (err) {
            console.log("error passing file to hdfs")
        });

        remoteFileStream.on('finish', function onFinish () {
            console.log("finished")
        });

        remoteFileStream.on('data', function onChunk (chunk) {
            // Concat received data chunk
            console.log("chunk received")
        });

    }
}
