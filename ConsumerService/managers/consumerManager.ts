/**
 * Created by odedl on 08-May-17.
 */
import {Consumer,Client} from "kafka-node"
import {IMessageMeta} from "../../Common/dataModels/messageMeta";
import {hdfs, HdfsActions} from "./hdfsManager";
import {JsonFileManager} from "./jsonFileManager";

export class ConsumerManager{
    consumer:Consumer;
    topic:string;
    counter:number=0;
    kafkaBroker:string;
    fileSaveInterval=0;
    messageQueue:IMessageMeta[] = [];
    saveOnHdfs:boolean;
    hdfsPathToSave:string;

    constructor(){
        this.initParamsFromEnv()

        this.initConsumer();
        this.initFileSchedular();
    }

    initParamsFromEnv=()=>{
        this.topic=process.env.TOPIC || "test";
        this.kafkaBroker=process.env.BROKER || "localhost:2181";
        this.fileSaveInterval = parseInt(process.env.FILE_INTERVAL||3000);
        this.saveOnHdfs=process.env.SAVE_ON_DHFS=="true";
        this.hdfsPathToSave=process.env.HDFS_SAVE_PATH || "/consumer/tmp";
    }

    initConsumer=()=>{
        let client = new Client(this.kafkaBroker)
        this.consumer = new Consumer(client,[],{autoCommit:true})
        console.log(`working with kafka broker : ${this.kafkaBroker}`)
        this.consumer.addTopics([this.topic],(err,added)=>{
            if(err){
                console.log(err)
            }else{
                console.log(`consumer listening to topic ${added}`)
            }
        })
        this.consumer.on('message', this.onReceiveMessage)
        this.consumer.on("error",this.onConsumerError)
    }

    onReceiveMessage=(message)=>{
        if(message && message.value){
            let mlMessage:IMessageMeta=  JSON.parse(message.value)
            this.messageQueue.push(mlMessage);
        }

    }

    onConsumerError = (err)=>{
        console.log(err)
    }

    initFileSchedular=()=>{
        setInterval(()=>{
            //send a copy for saving and clear queue
            if(this.messageQueue && this.messageQueue.length) {
                let count = this.messageQueue.length;
                let saveRes = new JsonFileManager().saveJson([...this.messageQueue], "consumerContent")
                this.messageQueue = [];
                console.log(`saving ${count} messages  to local file ${saveRes.fullPath}`)
                if(this.saveOnHdfs){
                    let pathToSave = `${this.hdfsPathToSave}/${saveRes.fileName}`
                    HdfsActions.exportFile(saveRes.fullPath, pathToSave)
                }
            }

        },this.fileSaveInterval)
    }


}